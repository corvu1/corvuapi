package com.corvu.corvuApi.webService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.corvu.corvuApi.entity.Test;
import com.corvu.corvuApi.repository.TestRepository;

@RestController
@RequestMapping("/public")
public class TestWebService {
	
	@Autowired
	private TestRepository testRepo;

	@GetMapping("/test")
	public Long test() {
		return 1L;
	}
	
	@GetMapping("/test2")
	public Long test2() {
		return 2L;
	}
	
	@GetMapping("/db")
	public Test db() {
		return testRepo.findById(0L).orElse(null);
	}
	
	@GetMapping("/dba")
	public List<Test>dba() {
		return testRepo.findAll();
	}
	
	@PostMapping("/db")
	public Test pDb(@RequestBody Test test) {
		return testRepo.save(test);
	}
}
