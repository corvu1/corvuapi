package com.corvu.corvuApi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.corvu.corvuApi.entity.Test;

@Repository
public interface TestRepository extends JpaRepository<Test, Long> {

	Optional<Test> findById(Long id);
	
}
