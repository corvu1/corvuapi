package com.corvu.corvuApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorvuApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CorvuApiApplication.class, args);
	}

}
